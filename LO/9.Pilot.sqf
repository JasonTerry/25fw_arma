comment "25th MEU Pilot";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "CUP_U_B_USMC_PilotOverall";
for "_i" from 1 to 10 do {_unit addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 10 do {_unit addItemToUniform "ACE_quikclot";};
_unit addItemToUniform "ACE_EarPlugs";
for "_i" from 1 to 4 do {_unit addItemToUniform "ACE_tourniquet";};
for "_i" from 1 to 5 do {_unit addItemToUniform "ACE_packingBandage";};
_unit addHeadgear "rhsusf_hgu56p_mask";
_unit addVest "CUP_V_B_PilotVest";
for _i from 1 to 2 do {_unit addItemToVest "CUP_15Rnd_9x19_M9";};

comment "Add weapons";
_unit addWeapon "CUP_hgun_M9";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_anprc152_3";
_unit linkItem "ItemcTab";
_unit linkItem "rhsusf_ANPVS_15";
